﻿using System.Linq;
using System.Threading.Tasks;
using JeyamBillingApp.Business.Interfaces;
using JeyamBillingApp.Web.Model;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JeyamBillingApp.Web.Areas.Customers.Controllers
{
    [Area("Customers")]
    [Authorize]
    public class SitesInfoController : Controller
    {

        private readonly ISiteService _siteService;

        public SitesInfoController(ISiteService siteService)
        {
            _siteService = siteService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            var result = _siteService.GetSites();
            return Json(result.ToDataSourceResult(request));
        }

        [HttpPost]
        public ActionResult Add([DataSourceRequest] DataSourceRequest request, SiteViewModel siteViewModel)
        {
            if (siteViewModel != null && ModelState.IsValid)
            {
                if (ModelState.IsValid)
                {
                    siteViewModel = _siteService.Add(siteViewModel);
                }
            }
            return Json(new[] { siteViewModel }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public ActionResult Edit([DataSourceRequest] DataSourceRequest request, SiteViewModel siteViewModel)
        {
            if (siteViewModel != null && ModelState.IsValid)
            {
                if (ModelState.IsValid)
                {
                    siteViewModel = _siteService.Update(siteViewModel);
                }
            }
            return Json(new[] { siteViewModel }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public ActionResult Delete([DataSourceRequest] DataSourceRequest request, SiteViewModel siteViewModel)
        {
            if (siteViewModel != null && ModelState.IsValid)
            {
                if (ModelState.IsValid)
                {
                    _siteService.Delete(siteViewModel);
                }
            }
            return Json(new[] { siteViewModel }.ToDataSourceResult(request, ModelState));
        }
    }
}