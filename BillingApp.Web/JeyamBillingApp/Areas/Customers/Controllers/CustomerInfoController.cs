﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JeyamBillingApp.Business.Interfaces;
using JeyamBillingApp.Web.Model;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JeyamBillingApp.Web.Areas.Customers.Controllers
{
    [Area("Customers")]
    [Authorize]
    public class CustomersInfoController : Controller
    {
        private readonly ICustomerService _customerService;

        public CustomersInfoController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            var result = _customerService.GetCustomers();
            return Json(result.ToDataSourceResult(request));
        }

        [HttpPost]
        public ActionResult Add([DataSourceRequest] DataSourceRequest request, CustomerViewModel customerViewModel)
        {
            if (customerViewModel != null && ModelState.IsValid)
            {
                if (ModelState.IsValid)
                {
                    customerViewModel = _customerService.Add(customerViewModel);
                }
            }
            return Json(new[] { customerViewModel }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public ActionResult Edit([DataSourceRequest] DataSourceRequest request, CustomerViewModel customerViewModel)
        {
            if (customerViewModel != null && ModelState.IsValid)
            {
                if (ModelState.IsValid)
                {
                    customerViewModel = _customerService.Update(customerViewModel);
                }
            }
            return Json(new[] { customerViewModel }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public ActionResult Delete([DataSourceRequest] DataSourceRequest request, CustomerViewModel customerViewModel)
        {
            if (customerViewModel != null && ModelState.IsValid)
            {
                if (ModelState.IsValid)
                {
                    _customerService.Delete(customerViewModel);
                }
            }
            return Json(new[] { customerViewModel }.ToDataSourceResult(request, ModelState));
        }
    }
}