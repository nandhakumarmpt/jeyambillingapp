﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JeyamBillingApp.Business.Interfaces;
using JeyamBillingApp.Web.Model;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JeyamBillingApp.Web.Areas.Vechicles.Controllers
{
    [Area("Vechicles")]
    [Authorize]
    public class VechiclesInfoController : Controller
    {
        private readonly IVechicleService _vechicleService;

        public VechiclesInfoController(IVechicleService vechicleService)
        {
            _vechicleService = vechicleService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            var result = _vechicleService.GetVechicles();
            return Json(result.ToDataSourceResult(request));
        }

        [HttpPost]
        public ActionResult Add([DataSourceRequest] DataSourceRequest request, VechicleViewModel vechicleViewModel)
        {
            if (vechicleViewModel != null && ModelState.IsValid)
            {
                if (ModelState.IsValid)
                {
                    vechicleViewModel = _vechicleService.Add(vechicleViewModel);
                }
            }
            return Json(new[] { vechicleViewModel }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public ActionResult Edit([DataSourceRequest] DataSourceRequest request, VechicleViewModel vechicleViewModel)
        {
            if (vechicleViewModel != null && ModelState.IsValid)
            {
                if (ModelState.IsValid)
                {
                    vechicleViewModel = _vechicleService.Update(vechicleViewModel);
                }
            }
            return Json(new[] { vechicleViewModel }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public ActionResult Delete([DataSourceRequest] DataSourceRequest request, VechicleViewModel vechicleViewModel)
        {
            if (vechicleViewModel != null && ModelState.IsValid)
            {
                if (ModelState.IsValid)
                {
                    _vechicleService.Delete(vechicleViewModel.VechicleId);
                }
            }
            return Json(new[] { vechicleViewModel }.ToDataSourceResult(request, ModelState));
        }
    }
}