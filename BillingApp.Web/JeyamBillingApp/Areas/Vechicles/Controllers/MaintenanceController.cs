﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JeyamBillingApp.Business.Interfaces;
using JeyamBillingApp.Web.Model;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
 

namespace JeyamBillingApp.Web.Areas.Vechicles.Controllers
{
    [Area("Vechicles")]
    [Authorize]
    public class MaintenanceController : Controller
    {

        private readonly IVechicleMaintenanceService _vechicleMaintenanceService;

        public MaintenanceController(IVechicleMaintenanceService vechicleMaintenanceService)
        {
            _vechicleMaintenanceService = vechicleMaintenanceService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            var result = _vechicleMaintenanceService.GetVechicleMaintenances();

            foreach(var maintenance in result)
            {
                maintenance.DateText = maintenance.Date.ToString("dd/MM/yyyy");
            }

            return Json(result.ToDataSourceResult(request));
        }

        [HttpPost]
        public ActionResult Add([DataSourceRequest] DataSourceRequest request, VechicleMaintenanceViewModel vechicleMaintenanceViewModel)
        {
            if (vechicleMaintenanceViewModel != null && ModelState.IsValid)
            {
                if (ModelState.IsValid)
                {
                    vechicleMaintenanceViewModel = _vechicleMaintenanceService.Add(vechicleMaintenanceViewModel);

                    vechicleMaintenanceViewModel.DateText = vechicleMaintenanceViewModel.Date.ToString("dd/MM/yyyy");
                }
            }
            return Json(new[] { vechicleMaintenanceViewModel }.ToDataSourceResult(request, ModelState));
        }


        [HttpPost]
        public ActionResult Edit([DataSourceRequest] DataSourceRequest request, VechicleMaintenanceViewModel vechicleMaintenanceViewModel)
        {
            if (vechicleMaintenanceViewModel != null && ModelState.IsValid)
            {
                if (ModelState.IsValid)
                {
                    vechicleMaintenanceViewModel = _vechicleMaintenanceService.Update(vechicleMaintenanceViewModel);

                    vechicleMaintenanceViewModel.DateText = vechicleMaintenanceViewModel.Date.ToString("dd/MM/yyyy");
                }
            }
            return Json(new[] { vechicleMaintenanceViewModel }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public ActionResult Delete([DataSourceRequest] DataSourceRequest request, VechicleMaintenanceViewModel vechicleMaintenanceViewModel)
        {
            if (vechicleMaintenanceViewModel != null && ModelState.IsValid)
            {
                if (ModelState.IsValid)
                {
                   _vechicleMaintenanceService.Delete(vechicleMaintenanceViewModel);
                }
            }
            return Json(new[] { vechicleMaintenanceViewModel }.ToDataSourceResult(request, ModelState));
        }

        public JsonResult GetVechicleList()
        {
            var result = _vechicleMaintenanceService.GetVechiclesList();

            return Json(result);
        }
    }
}