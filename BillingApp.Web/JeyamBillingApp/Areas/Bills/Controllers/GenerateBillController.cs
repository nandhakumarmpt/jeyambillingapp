﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JeyamBillingApp.Web.Areas.Bills.Controllers
{
    [Area("Bills")]
    [Authorize]
    public class GenerateBillController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}