﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JeyamBillingApp.Business.Interfaces;
using JeyamBillingApp.Web.Model;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JeyamBillingApp.Web.Areas.Bills.Controllers
{
    [Area("Bills")]
    [Authorize]
    public class DailyWorkController : Controller
    {
        private readonly IDailyWorkService _dailyWorkService;

        public DailyWorkController(IDailyWorkService dailyWorkService)
        {
            _dailyWorkService = dailyWorkService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            var result = _dailyWorkService.GetDailyWorks();
            if (result.Any())
            {
                foreach (var work in result)
                {
                    work.TotalHoursText = getTimeDiffrence(work.StartTime, work.EndTime);
                }
            }
            return Json(result.ToDataSourceResult(request));
        }

        [HttpPost]
        public ActionResult Add([DataSourceRequest] DataSourceRequest request, DailyWorkViewModel dailyWorkViewModel)
        {
            if (dailyWorkViewModel != null && ModelState.IsValid)
            {
                if (ModelState.IsValid)
                {
                    dailyWorkViewModel = _dailyWorkService.Add(dailyWorkViewModel);

                    dailyWorkViewModel.TotalHoursText = getTimeDiffrence(dailyWorkViewModel.StartTime, dailyWorkViewModel.EndTime);
                }
            }
            return Json(new[] { dailyWorkViewModel }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public ActionResult Edit([DataSourceRequest] DataSourceRequest request, DailyWorkViewModel dailyWorkViewModel)
        {
            if (dailyWorkViewModel != null && ModelState.IsValid)
            {
                if (ModelState.IsValid)
                {
                    dailyWorkViewModel = _dailyWorkService.Update(dailyWorkViewModel);

                    dailyWorkViewModel.TotalHoursText = getTimeDiffrence(dailyWorkViewModel.StartTime, dailyWorkViewModel.EndTime);
                }
            }
            return Json(new[] { dailyWorkViewModel }.ToDataSourceResult(request, ModelState));
        }


        [HttpPost]
        public ActionResult Delete([DataSourceRequest] DataSourceRequest request, DailyWorkViewModel dailyWorkViewModel)
        {
            if (dailyWorkViewModel != null && ModelState.IsValid)
            {
                if (ModelState.IsValid)
                {
                    _dailyWorkService.Delete(dailyWorkViewModel);
                }
            }
            return Json(new[] { dailyWorkViewModel }.ToDataSourceResult(request, ModelState));
        }

        public JsonResult GetSitesNameList()
        {
            return Json(_dailyWorkService.GetSitesName());
        }

        public string getTimeDiffrence(DateTime startDate, DateTime endDate)
        {
            var _eventDurationHours = Convert.ToInt32(((endDate.Subtract(startDate).Days * 24) + (endDate.Subtract(startDate)).Hours));

            var _eventDurationMinutes = endDate.Subtract(startDate).Minutes;

            var _eventDurationText = $"{_eventDurationHours}h {_eventDurationMinutes}m";

            return _eventDurationText;
        }

        public JsonResult GetTimeDiffrenceText(DateTime startDate, DateTime endDate)
        {
            var _eventDurationText = getTimeDiffrence(startDate, endDate);

            var _span = getTimeDiffrenceDecimal(startDate, endDate);

            return Json(new { eventDurationText = _eventDurationText, eventDuration = _span });
        }

        public double getTimeDiffrenceDecimal(DateTime startDate, DateTime endDate)
        {
            return (endDate - startDate).TotalHours;
        }
    }
}