﻿using Microsoft.AspNetCore.Hosting;

[assembly: HostingStartup(typeof(JeyamBillingApp.Web.Areas.Identity.IdentityHostingStartup))]
namespace JeyamBillingApp.Web.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
            });
        }
    }
}