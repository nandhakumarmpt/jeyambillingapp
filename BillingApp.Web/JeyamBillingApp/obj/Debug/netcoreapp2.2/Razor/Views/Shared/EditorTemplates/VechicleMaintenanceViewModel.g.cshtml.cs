#pragma checksum "C:\Users\nandh\Documents\Source\Repos\JeyamBillingApp\BillingApp.Web\JeyamBillingApp\Views\Shared\EditorTemplates\VechicleMaintenanceViewModel.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "0edd5fc9b4ada4712935a41fdc5ba212e29649aa"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared_EditorTemplates_VechicleMaintenanceViewModel), @"mvc.1.0.view", @"/Views/Shared/EditorTemplates/VechicleMaintenanceViewModel.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Shared/EditorTemplates/VechicleMaintenanceViewModel.cshtml", typeof(AspNetCore.Views_Shared_EditorTemplates_VechicleMaintenanceViewModel))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\nandh\Documents\Source\Repos\JeyamBillingApp\BillingApp.Web\JeyamBillingApp\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
#line 2 "C:\Users\nandh\Documents\Source\Repos\JeyamBillingApp\BillingApp.Web\JeyamBillingApp\Views\_ViewImports.cshtml"
using JeyamBillingApp.Data;

#line default
#line hidden
#line 3 "C:\Users\nandh\Documents\Source\Repos\JeyamBillingApp\BillingApp.Web\JeyamBillingApp\Views\_ViewImports.cshtml"
using JeyamBillingApp.Common;

#line default
#line hidden
#line 6 "C:\Users\nandh\Documents\Source\Repos\JeyamBillingApp\BillingApp.Web\JeyamBillingApp\Views\_ViewImports.cshtml"
using JeyamBillingApp.Web;

#line default
#line hidden
#line 7 "C:\Users\nandh\Documents\Source\Repos\JeyamBillingApp\BillingApp.Web\JeyamBillingApp\Views\_ViewImports.cshtml"
using JeyamBillingApp.Web.Models;

#line default
#line hidden
#line 8 "C:\Users\nandh\Documents\Source\Repos\JeyamBillingApp\BillingApp.Web\JeyamBillingApp\Views\_ViewImports.cshtml"
using JeyamBillingApp.Data.Database;

#line default
#line hidden
#line 11 "C:\Users\nandh\Documents\Source\Repos\JeyamBillingApp\BillingApp.Web\JeyamBillingApp\Views\_ViewImports.cshtml"
using Kendo.Mvc.UI;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"0edd5fc9b4ada4712935a41fdc5ba212e29649aa", @"/Views/Shared/EditorTemplates/VechicleMaintenanceViewModel.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"a0e101687b633ef0bf0b73774b9b1ab93a58422a", @"/Views/_ViewImports.cshtml")]
    public class Views_Shared_EditorTemplates_VechicleMaintenanceViewModel : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(0, 225, true);
            WriteLiteral("\r\n\r\n\r\n<input type=\"hidden\" id=\"VechicleMaintenanceId\" name=\"VechicleMaintenanceId\" />\r\n\r\n<div class=\"form-group row required\">\r\n    <label class=\"col-md-2 col-form-label\">Vechicle</label>\r\n    <div class=\"col-md-9\">\r\n        ");
            EndContext();
            BeginContext(227, 646, false);
#line 9 "C:\Users\nandh\Documents\Source\Repos\JeyamBillingApp\BillingApp.Web\JeyamBillingApp\Views\Shared\EditorTemplates\VechicleMaintenanceViewModel.cshtml"
    Write(Html.Kendo().DropDownList()
        .OptionLabel("Please select a value")
        .Name("VechicleId")
        .DataTextField("Text")
        .DataValueField("Id")
        .Events(e => e.Change("change_vechicleId"))
        .DataSource(source =>
        {
            source.Read(read =>
            {
                read.Action("GetVechicleList", "Maintenance");
            });
        })

        .HtmlAttributes(new
        {
            reqiored = "required",

             style = "width: 100%",

            data_bind = "value:VechicleId",
            data_val_required = "The field is required."
        })
        );

#line default
#line hidden
            EndContext();
            BeginContext(874, 270, true);
            WriteLiteral(@"
        <span class=""field-validation-valid"" data-valmsg-for=""VechicleId"" data-valmsg-replace=""true""></span>
    </div>
</div>



<div class=""form-group row required"">
    <label class=""col-md-2 col-form-label"">Date</label>
    <div class=""col-md-9"">
        ");
            EndContext();
            BeginContext(1146, 284, false);
#line 42 "C:\Users\nandh\Documents\Source\Repos\JeyamBillingApp\BillingApp.Web\JeyamBillingApp\Views\Shared\EditorTemplates\VechicleMaintenanceViewModel.cshtml"
    Write(Html.Kendo().DatePicker()
        .Name("Date").Format("dd/MM/yyyy")
        .HtmlAttributes(new
        {
        reqiored = "required",
       style = "width: 180px",
        data_bind = "value:Date",
        data_val_required = "The field is required."
        })
        );

#line default
#line hidden
            EndContext();
            BeginContext(1431, 262, true);
            WriteLiteral(@"
        <span class=""field-validation-valid"" data-valmsg-for=""Date"" data-valmsg-replace=""true""></span>
    </div>
</div>

<div class=""form-group row required"">
    <label class=""col-md-2 col-form-label"">Amount</label>
    <div class=""col-md-9"">
        ");
            EndContext();
            BeginContext(1695, 291, false);
#line 59 "C:\Users\nandh\Documents\Source\Repos\JeyamBillingApp\BillingApp.Web\JeyamBillingApp\Views\Shared\EditorTemplates\VechicleMaintenanceViewModel.cshtml"
    Write(Html.Kendo().NumericTextBox()
        .Name("Amount").Format("c").Min(1)
        .HtmlAttributes(new
        {
        reqiored = "required",
        style = "width: 180px",
        data_bind = "value:Amount",
        data_val_required = "The field is required."
        })
        );

#line default
#line hidden
            EndContext();
            BeginContext(1987, 265, true);
            WriteLiteral(@"
        <span class=""field-validation-valid"" data-valmsg-for=""Amount"" data-valmsg-replace=""true""></span>
    </div>
 </div>

<div class=""form-group row required"">
    <label class=""col-md-2 col-form-label"">Note</label>
    <div class=""col-md-9"">

        ");
            EndContext();
            BeginContext(2254, 280, false);
#line 77 "C:\Users\nandh\Documents\Source\Repos\JeyamBillingApp\BillingApp.Web\JeyamBillingApp\Views\Shared\EditorTemplates\VechicleMaintenanceViewModel.cshtml"
    Write(Html.TextArea("Note", "", new
        {
        @class = "k-textbox",
        style = "width: 100%; height: 64px !important",
        rows="4",
        data_bind = "value:Note",
        data_val_required = "The field is required.",
        reqiored = "required"
        }));

#line default
#line hidden
            EndContext();
            BeginContext(2535, 130, true);
            WriteLiteral("\r\n\r\n        <span class=\"field-validation-valid\" data-valmsg-for=\"Note\" data-valmsg-replace=\"true\"></span>\r\n    </div>\r\n</div>\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public UserManager<ApplicationUser> UserManager { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public SignInManager<ApplicationUser> SignInManager { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
