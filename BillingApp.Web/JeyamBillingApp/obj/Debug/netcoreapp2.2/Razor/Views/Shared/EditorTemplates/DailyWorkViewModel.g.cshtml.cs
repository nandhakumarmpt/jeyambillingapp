#pragma checksum "C:\Users\nandh\Documents\Source\Repos\JeyamBillingApp\BillingApp.Web\JeyamBillingApp\Views\Shared\EditorTemplates\DailyWorkViewModel.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "fdf6270015959bd28ba86d1161ffbaa2203e63f3"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared_EditorTemplates_DailyWorkViewModel), @"mvc.1.0.view", @"/Views/Shared/EditorTemplates/DailyWorkViewModel.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Shared/EditorTemplates/DailyWorkViewModel.cshtml", typeof(AspNetCore.Views_Shared_EditorTemplates_DailyWorkViewModel))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\nandh\Documents\Source\Repos\JeyamBillingApp\BillingApp.Web\JeyamBillingApp\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
#line 2 "C:\Users\nandh\Documents\Source\Repos\JeyamBillingApp\BillingApp.Web\JeyamBillingApp\Views\_ViewImports.cshtml"
using JeyamBillingApp.Data;

#line default
#line hidden
#line 3 "C:\Users\nandh\Documents\Source\Repos\JeyamBillingApp\BillingApp.Web\JeyamBillingApp\Views\_ViewImports.cshtml"
using JeyamBillingApp.Common;

#line default
#line hidden
#line 6 "C:\Users\nandh\Documents\Source\Repos\JeyamBillingApp\BillingApp.Web\JeyamBillingApp\Views\_ViewImports.cshtml"
using JeyamBillingApp.Web;

#line default
#line hidden
#line 7 "C:\Users\nandh\Documents\Source\Repos\JeyamBillingApp\BillingApp.Web\JeyamBillingApp\Views\_ViewImports.cshtml"
using JeyamBillingApp.Web.Models;

#line default
#line hidden
#line 8 "C:\Users\nandh\Documents\Source\Repos\JeyamBillingApp\BillingApp.Web\JeyamBillingApp\Views\_ViewImports.cshtml"
using JeyamBillingApp.Data.Database;

#line default
#line hidden
#line 11 "C:\Users\nandh\Documents\Source\Repos\JeyamBillingApp\BillingApp.Web\JeyamBillingApp\Views\_ViewImports.cshtml"
using Kendo.Mvc.UI;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"fdf6270015959bd28ba86d1161ffbaa2203e63f3", @"/Views/Shared/EditorTemplates/DailyWorkViewModel.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"a0e101687b633ef0bf0b73774b9b1ab93a58422a", @"/Views/_ViewImports.cshtml")]
    public class Views_Shared_EditorTemplates_DailyWorkViewModel : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(0, 242, true);
            WriteLiteral("<input type=\"hidden\" id=\"DailyWorkId\" name=\"DailyWorkId\" />\r\n<input type=\"hidden\" id=\"BillId\" name=\"BillId\" />\r\n\r\n<div class=\"form-group row\">\r\n    <label class=\"col-md-2 col-form-label\">Vechicle</label>\r\n    <div class=\"col-md-10\">\r\n        ");
            EndContext();
            BeginContext(244, 654, false);
#line 7 "C:\Users\nandh\Documents\Source\Repos\JeyamBillingApp\BillingApp.Web\JeyamBillingApp\Views\Shared\EditorTemplates\DailyWorkViewModel.cshtml"
    Write(Html.Kendo().DropDownList()
       .OptionLabel("Please select a value")
       .Name("VechicleId")
       .DataTextField("Text")
       .DataValueField("Id")
       .Events(e => e.Change("change_vechicleId"))
       .DataSource(source =>
       {
           source.Read(read =>
           {
               read.Action("GetVechicleList", "Maintenance", new { area="Vechicles" });
           });
       })

       .HtmlAttributes(new
       {
           reqiored = "required",
           style = "width:100%;max-width:300px;",
           data_bind = "value:VechicleId",
           data_val_required = "The field is required."
       }));

#line default
#line hidden
            EndContext();
            BeginContext(899, 272, true);
            WriteLiteral(@"
        <span class=""field-validation-valid"" data-valmsg-for=""VechicleId"" data-valmsg-replace=""true""></span>
    </div>
</div>

<div class=""form-group row required"">
    <label class=""col-md-2 col-form-label"">Site Name</label>
    <div class=""col-md-10"">
        ");
            EndContext();
            BeginContext(1173, 573, false);
#line 35 "C:\Users\nandh\Documents\Source\Repos\JeyamBillingApp\BillingApp.Web\JeyamBillingApp\Views\Shared\EditorTemplates\DailyWorkViewModel.cshtml"
    Write(Html.Kendo().DropDownList()
      .OptionLabel("Please select a value")
      .Name("SiteId")
      .DataTextField("Text")
      .DataValueField("Id")
      .DataSource(source =>
      {
          source.Read(read =>
          {
              read.Action("GetSitesNameList", "DailyWork", new { area = "Bills" });
          });
      })

      .HtmlAttributes(new
      {
          reqiored = "required",
          style = "width:100%;max-width:300px;",
          data_bind = "value:SiteId",
          data_val_required = "The field is required."
      }));

#line default
#line hidden
            EndContext();
            BeginContext(1747, 262, true);
            WriteLiteral(@"
        <span class=""field-validation-valid"" data-valmsg-for=""SiteId"" data-valmsg-replace=""true""></span>
    </div>
</div>

<div class=""form-group row required"">
    <label class=""col-md-2 col-form-label"">Date</label>
    <div class=""col-md-9"">
        ");
            EndContext();
            BeginContext(2011, 285, false);
#line 62 "C:\Users\nandh\Documents\Source\Repos\JeyamBillingApp\BillingApp.Web\JeyamBillingApp\Views\Shared\EditorTemplates\DailyWorkViewModel.cshtml"
    Write(Html.Kendo().DatePicker()
        .Name("Date").Format("dd/MM/yyyy")
        .HtmlAttributes(new
        {
        reqiored = "required",
        style = "width: 180px",
        data_bind = "value:Date",
        data_val_required = "The field is required."
        })
        );

#line default
#line hidden
            EndContext();
            BeginContext(2297, 266, true);
            WriteLiteral(@"
        <span class=""field-validation-valid"" data-valmsg-for=""Date"" data-valmsg-replace=""true""></span>
    </div>
</div>

<div class=""form-group row required"">
    <label class=""col-md-2 col-form-label"">Start Time</label>
    <div class=""col-md-9"">
        ");
            EndContext();
            BeginContext(2565, 360, false);
#line 79 "C:\Users\nandh\Documents\Source\Repos\JeyamBillingApp\BillingApp.Web\JeyamBillingApp\Views\Shared\EditorTemplates\DailyWorkViewModel.cshtml"
    Write(Html.Kendo().TimePicker().Events(e=>e.Change("durationChanged"))
        .Name("StartTime").Interval(5).Animation(true).Format("hh:mm tt")
        .HtmlAttributes(new
        {
        reqiored = "required",
        style = "width: 180px",
        data_bind = "value:StartTime",
        data_val_required = "The field is required."
        })
        );

#line default
#line hidden
            EndContext();
            BeginContext(2926, 269, true);
            WriteLiteral(@"
        <span class=""field-validation-valid"" data-valmsg-for=""StartTime"" data-valmsg-replace=""true""></span>
    </div>
</div>

<div class=""form-group row required"">
    <label class=""col-md-2 col-form-label"">End Time</label>
    <div class=""col-md-9"">
        ");
            EndContext();
            BeginContext(3197, 356, false);
#line 96 "C:\Users\nandh\Documents\Source\Repos\JeyamBillingApp\BillingApp.Web\JeyamBillingApp\Views\Shared\EditorTemplates\DailyWorkViewModel.cshtml"
    Write(Html.Kendo().TimePicker().Events(e=>e.Change("durationChanged"))
        .Name("EndTime").Interval(5).Animation(true).Format("hh:mm tt")
        .HtmlAttributes(new
        {
        reqiored = "required",
        style = "width: 180px",
        data_bind = "value:EndTime",
        data_val_required = "The field is required."
        })
        );

#line default
#line hidden
            EndContext();
            BeginContext(3554, 733, true);
            WriteLiteral(@"
        <span class=""field-validation-valid"" data-valmsg-for=""EndTime"" data-valmsg-replace=""true""></span>
    </div>
</div>

<div class=""form-group row required"">
    <label class=""col-md-2 col-form-label"">Total Hours</label>
    <div class=""col-md-9"">
        <input type=""text"" class=""k-textbox k-readonly"" style=""width: 180px;background-color:whitesmoke"" data-bind=""value:TotalHoursText"" readonly id=""TotalHoursText"" name=""TotalHoursText"" />
        <span class=""field-validation-valid"" data-valmsg-for=""TotalHoursText"" data-valmsg-replace=""true""></span>
    </div>
</div>

<div class=""form-group row required"">
    <label class=""col-md-2 col-form-label"">Advance Amount</label>
    <div class=""col-md-9"">
        ");
            EndContext();
            BeginContext(4289, 275, false);
#line 121 "C:\Users\nandh\Documents\Source\Repos\JeyamBillingApp\BillingApp.Web\JeyamBillingApp\Views\Shared\EditorTemplates\DailyWorkViewModel.cshtml"
    Write(Html.Kendo().NumericTextBox()
        .Name("AdvanceAmount").Format("c").Min(0)
        .HtmlAttributes(new
        {
        style = "width: 180px",
        data_bind = "value:AdvanceAmount",
        //data_val_required = "The field is required."
        })
        );

#line default
#line hidden
            EndContext();
            BeginContext(4565, 275, true);
            WriteLiteral(@"
        <span class=""field-validation-valid"" data-valmsg-for=""AdvanceAmount"" data-valmsg-replace=""true""></span>
    </div>
</div>

<div class=""form-group row required"">
    <label class=""col-md-2 col-form-label"">Disel (lt)</label>
    <div class=""col-md-9"">
        ");
            EndContext();
            BeginContext(4842, 257, false);
#line 137 "C:\Users\nandh\Documents\Source\Repos\JeyamBillingApp\BillingApp.Web\JeyamBillingApp\Views\Shared\EditorTemplates\DailyWorkViewModel.cshtml"
    Write(Html.Kendo().NumericTextBox()
        .Name("DiselLitre").Min(0)
        .HtmlAttributes(new
        {
        style = "width: 180px",
        data_bind = "value:DiselLitre",
        //data_val_required = "The field is required."
        })
        );

#line default
#line hidden
            EndContext();
            BeginContext(5100, 274, true);
            WriteLiteral(@"
        <span class=""field-validation-valid"" data-valmsg-for=""DiselLitre"" data-valmsg-replace=""true""></span>
    </div>
</div>

<div class=""form-group row required"">
    <label class=""col-md-2 col-form-label"">Disel Amount</label>
    <div class=""col-md-9"">
        ");
            EndContext();
            BeginContext(5376, 215, false);
#line 153 "C:\Users\nandh\Documents\Source\Repos\JeyamBillingApp\BillingApp.Web\JeyamBillingApp\Views\Shared\EditorTemplates\DailyWorkViewModel.cshtml"
    Write(Html.Kendo().NumericTextBox()
        .Name("DiselAmount").Format("c").Min(0)
        .HtmlAttributes(new
        {
        style = "width: 180px",
        data_bind = "value:DiselAmount",
        })
        );

#line default
#line hidden
            EndContext();
            BeginContext(5592, 131, true);
            WriteLiteral("\r\n        <span class=\"field-validation-valid\" data-valmsg-for=\"DiselAmount\" data-valmsg-replace=\"true\"></span>\r\n    </div>\r\n</div>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public UserManager<ApplicationUser> UserManager { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public SignInManager<ApplicationUser> SignInManager { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
