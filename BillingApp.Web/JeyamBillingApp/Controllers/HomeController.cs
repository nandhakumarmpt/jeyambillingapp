﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using JeyamBillingApp.Web.Models;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace JeyamBillingApp.Web.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

    }
}
