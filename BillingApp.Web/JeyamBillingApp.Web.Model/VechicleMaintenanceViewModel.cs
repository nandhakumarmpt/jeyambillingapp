﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JeyamBillingApp.Web.Model
{
    public class VechicleMaintenanceViewModel
    {
        public int VechicleMaintenanceId { get; set; }
        public int VechicleId { get; set; }
        public string VechicleNumber { get; set; }
        public DateTime Date { get; set; }
        public string DateText { get; set; }
        public double Amount { get; set; }
        public string Note { get; set; }
    }
}
