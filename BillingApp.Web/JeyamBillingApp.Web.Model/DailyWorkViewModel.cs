﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace JeyamBillingApp.Web.Model
{
    public class DailyWorkViewModel
    {
        public int DailyWorkId { get; set; }

        public int? BillId { get; set; }

        [Required]
        public int VechicleId { get; set; }

        [Required]
        public short SiteId { get; set; }

        public string SiteName { get; set; }

        public string VechicleNumber { get; set; }

        [Required]
        public DateTime Date { get; set; }

        public string DateText { get; set; }

        [Required]
        public DateTime StartTime { get; set; }

        [Required]
        public DateTime EndTime { get; set; }

        public double TotalHours { get; set; }

        public string TotalHoursText { get; set; }

        public double? AdvanceAmount { get; set; }

        public double? DiselLitre { get; set; }

        public double? DiselAmount { get; set; }

    }
}
