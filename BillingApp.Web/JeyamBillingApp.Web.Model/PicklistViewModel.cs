﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JeyamBillingApp.Web.Model
{
   public class PicklistViewModel
    {
        public int Id { get; set; }

        public string Text { get; set; }
    }
}
