﻿using System;
using System.ComponentModel.DataAnnotations;

namespace JeyamBillingApp.Web.Model
{
    public class VechicleViewModel
    {

        public int VechicleId { get; set; }

        [Display(Name = "Vechicle Number")]
        [Required]
        public string VechicleNumber { get; set; }
    }
}
