﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace JeyamBillingApp.Web.Model
{
    public class BillViewModel
    {
        public int Id { get; set; }

        public int? BillNo { get; set; }

        [Required]
        public double HourlyRate { get; set; }

        public int?  MonthlyHoursLimit { get; set; }

        [Required]
        public bool IsMonthlyBill { get; set; }

        public bool IsPaymentCompleted { get; set; }

    }
}
