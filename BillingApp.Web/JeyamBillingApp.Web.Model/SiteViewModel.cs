﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace JeyamBillingApp.Web.Model
{
    public class SiteViewModel
    {
        public short SiteId { get; set; }

        [Required]
        [MaxLength(250, ErrorMessage = "Site Name cannot be longer than 250 characters.")]
        public string Name { get; set; }
    }
}
