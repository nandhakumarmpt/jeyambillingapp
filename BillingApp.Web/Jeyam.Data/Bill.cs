﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace JeyamBillingApp.Data
{
    public class Bill
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public short Id { get; set; }

        public string SiteName { get; set; }

        public bool IsMonthlyBill { get; set; }

        public double HourlyRate { get; set; }

        public Int16 MonthlyHoursLimit { get; set; }

        public bool IsPaymentDone { get; set; }

        public ICollection<DailyWork> DailyWork { get; set; }
    }
}
