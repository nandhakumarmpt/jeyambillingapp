﻿using JeyamBillingApp.Data.Database;
using JeyamBillingApp.Data.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JeyamBillingApp.Data.Repository
{
    public class CustomerRepository : ICustomerRepository, IDisposable
    {
        private readonly ApplicationDbContext _dbContext;

        public CustomerRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Customer Add(Customer customer)
        {
            _dbContext.Customers.Add(customer);
            Save();
            return customer;
        }

        public IQueryable<Customer> GetCustomers()
        {
            return _dbContext.Customers;
        }

        public Customer Update(Customer customer)
        {
            _dbContext.Entry(customer).State = EntityState.Modified;
            Save();
            return customer;
        }

        public void Delete(Customer customer)
        {
            _dbContext.Remove(customer);
            Save();
        }


        public void Save()
        {
            _dbContext.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

       
    }
}
