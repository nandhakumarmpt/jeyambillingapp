﻿using JeyamBillingApp.Data.Database;
using JeyamBillingApp.Data.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JeyamBillingApp.Data.Repository
{
    public class DailyWorkRepository : IDailyWorkRepository, IDisposable
    {

        private readonly ApplicationDbContext _dbContext;

        public DailyWorkRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IQueryable<DailyWork> GetDailyWorks()
        {
            return _dbContext.DailyWork;
        }

        public DailyWork Add(DailyWork dailyWork)
        {
            _dbContext.DailyWork.Add(dailyWork);
            Save();
            return dailyWork;
        }

        public DailyWork Update(DailyWork dailyWork)
        {
            _dbContext.Entry(dailyWork).State = EntityState.Modified;
            Save();
            return dailyWork;
        }

        public DailyWork Delete(DailyWork dailyWork)
        {
            _dbContext.Remove(dailyWork);
            Save();
            return dailyWork;
        }


        public void Save()
        {
            _dbContext.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
