﻿using JeyamBillingApp.Data.Database;
using JeyamBillingApp.Data.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JeyamBillingApp.Data.Repository
{
    public class VechicleMaintenanceRepository : IVechicleMaintenanceRepository, IDisposable
    {
        private readonly ApplicationDbContext _dbContext;

        public VechicleMaintenanceRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public VechicleMaintenance Add(VechicleMaintenance vechicleMaintenance)
        {
            if (vechicleMaintenance != null)
            {
                _dbContext.VechicleMaintenances.Add(vechicleMaintenance);
                Save();
            }

            return vechicleMaintenance;
        }

        public void Delete(VechicleMaintenance vechicleMaintenance)
        {
            if (vechicleMaintenance != null)
            {
                _dbContext.Remove(vechicleMaintenance);
                Save();
            }
        }

        public IQueryable<VechicleMaintenance> GetVechicleMaintenances()
        {
            return _dbContext.VechicleMaintenances;
        }

        public VechicleMaintenance Update(VechicleMaintenance vechicleMaintenance)
        {
            if (vechicleMaintenance != null)
            {
                _dbContext.Entry(vechicleMaintenance).State = EntityState.Modified;
                Save();
            }

            return vechicleMaintenance;
        }


        public void Save()
        {
            _dbContext.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
