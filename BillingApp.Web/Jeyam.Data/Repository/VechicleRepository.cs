﻿using Jeyam.Data;
using JeyamBillingApp.Data.Database;
using JeyamBillingApp.Data.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace JeyamBillingApp.Data.Repository
{
    public class VechicleRepository : IVechicleRepository, IDisposable
    {
        private readonly ApplicationDbContext _dbContext;

        public VechicleRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Vechicle GetVechicle(int vechicleId)
        {
            return (_dbContext.Vechicles.FirstOrDefault(s => s.Id == vechicleId));
        }

        public IQueryable<Vechicle> GetVechicles()
        {
            return (_dbContext.Vechicles);
        }

        public Vechicle Add(Vechicle vechicle)
        {
            _dbContext.Vechicles.Add(vechicle);
            Save();
            return vechicle;
        }

        public Vechicle Update(Vechicle vechicle)
        {
            _dbContext.Entry(vechicle).State = EntityState.Modified;
            Save();
            return vechicle;
        }

        public void Delete(Vechicle vechicle)
        {
            _dbContext.Remove(vechicle);
            Save();
        }

        public void Save()
        {
            _dbContext.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
