﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JeyamBillingApp.Data.Repository.Interfaces
{
    public interface IVechicleMaintenanceRepository : IDisposable
    {

        IQueryable<VechicleMaintenance> GetVechicleMaintenances();

        VechicleMaintenance Add(VechicleMaintenance vechicleMaintenance);

        VechicleMaintenance Update(VechicleMaintenance vechicleMaintenance);

        void Delete(VechicleMaintenance vechicleMaintenance);
    }
}
