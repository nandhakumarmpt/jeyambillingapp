﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JeyamBillingApp.Data.Repository.Interfaces
{
    public interface IBillRepository : IDisposable
    {
        IQueryable<Bill> GetBills();

        Bill Add(Bill bill);

        Bill Update(Bill bill);

        Bill Delete(Bill bill);

        void Save();
    }
}
