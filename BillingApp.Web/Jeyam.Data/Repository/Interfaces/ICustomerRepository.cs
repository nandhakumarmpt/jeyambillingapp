﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JeyamBillingApp.Data.Repository.Interfaces
{
    public interface ICustomerRepository : IDisposable
    {
        IQueryable<Customer> GetCustomers();

        Customer Add(Customer customer);

        Customer Update(Customer customer);

        void Delete(Customer customer);

        void Save();
    }
}

