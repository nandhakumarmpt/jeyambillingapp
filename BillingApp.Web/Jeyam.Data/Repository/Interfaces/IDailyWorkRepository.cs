﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JeyamBillingApp.Data.Repository.Interfaces
{
    public interface IDailyWorkRepository :IDisposable
    {
        IQueryable<DailyWork> GetDailyWorks();

        DailyWork Add (DailyWork dailyWork);

        DailyWork Update(DailyWork dailyWork);

        DailyWork Delete(DailyWork dailyWork);

        void Save();
    }
}