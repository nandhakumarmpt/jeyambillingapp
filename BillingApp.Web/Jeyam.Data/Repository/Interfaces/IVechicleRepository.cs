﻿using Jeyam.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JeyamBillingApp.Data.Repository.Interfaces
{
    public interface IVechicleRepository : IDisposable
    {
        Vechicle GetVechicle(int vechicleId);

        IQueryable<Vechicle> GetVechicles();

        Vechicle Add(Vechicle vechicle);

        Vechicle Update(Vechicle vechicle);

        void Delete(Vechicle vechicle);


    }
}
