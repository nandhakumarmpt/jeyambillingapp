﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JeyamBillingApp.Data.Repository.Interfaces
{
    public interface ISiteRepository : IDisposable
    {
        IQueryable<Site> GetSites();

        Site Add(Site site);

        Site Update(Site site);

        void Delete(Site site);

        void Save();
    }
}

