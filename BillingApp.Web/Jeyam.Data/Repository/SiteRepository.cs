﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JeyamBillingApp.Data.Database;
using JeyamBillingApp.Data.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;


namespace JeyamBillingApp.Data.Repository
{
   public  class SiteRepository : ISiteRepository ,IDisposable
    {
        private readonly ApplicationDbContext _dbContext;

        public SiteRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IQueryable<Site> GetSites()
        {
            return _dbContext.Sites;
        }

        public Site Add(Site site)
        {
            _dbContext.Sites.Add(site);
            Save();
            return site;
        }

        public Site Update(Site site)
        {
            _dbContext.Entry(site).State = EntityState.Modified;
            Save();
            return site;
        }

        public void Delete(Site site)
        {
            _dbContext.Remove(site);
            Save();
        }

        public void Save()
        {
            _dbContext.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
