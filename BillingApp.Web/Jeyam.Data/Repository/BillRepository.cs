﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JeyamBillingApp.Data.Database;
using JeyamBillingApp.Data.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace JeyamBillingApp.Data.Repository
{
    public class BillRepository : IBillRepository, IDisposable
    {
        private readonly ApplicationDbContext _dbContext;

        public BillRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IQueryable<Bill> GetBills()
        {
            return _dbContext.Bills;
        }

        public Bill Add(Bill bill)
        {
            _dbContext.Bills.Add(bill);
            Save();
            return bill;
        }

        public Bill Update(Bill bill)
        {
            _dbContext.Entry(bill).State = EntityState.Modified;
            Save();
            return bill;
        }

        public Bill Delete(Bill bill)
        {
            _dbContext.Remove(bill);
            Save();
            return bill;
        }

        public void Save()
        {
            _dbContext.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}
