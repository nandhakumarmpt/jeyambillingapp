﻿using Jeyam.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace JeyamBillingApp.Data.Database
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public ApplicationDbContext()
       : base()
        {
        }

        public DbSet<Vechicle> Vechicles { get; set; }

        public DbSet<VechicleMaintenance> VechicleMaintenances { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<Bill> Bills { get; set; }

        public DbSet<DailyWork> DailyWork { get; set; }

        public DbSet<Site> Sites { get; set; }
    }


    public class ApplicationUser : IdentityUser
    {
        public string Name { get; set; }

        public string Image { get; set; }

    }
}
