﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace JeyamBillingApp.Data
{
    public class DailyWork
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("Bill")]
        public short? BillId { get; set; }

        [ForeignKey("Vechicle")]
        public int VechicleId { get; set; }

        [ForeignKey("Site")]
        public short SiteId { get; set; }

        public DateTime Date { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

       public double? AdvanceAmount { get; set; }

        public double? DiselLitre { get; set; }

        public double? DiselAmount { get; set; }
    }
}
