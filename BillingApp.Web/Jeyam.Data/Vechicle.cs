﻿using JeyamBillingApp.Data;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Jeyam.Data
{
    public class Vechicle
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Model { get; set; }

        public ICollection<VechicleMaintenance> VechicleMaintenance { get; set; }

        public ICollection<DailyWork> DailyWork { get; set; }
    }
}
