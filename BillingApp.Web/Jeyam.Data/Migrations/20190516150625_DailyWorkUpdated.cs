﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace JeyamBillingApp.Data.Migrations
{
    public partial class DailyWorkUpdated : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TotalHours",
                table: "DailyWork");

            migrationBuilder.DropColumn(
                name: "VechicleNumber",
                table: "DailyWork");

            migrationBuilder.AlterColumn<double>(
                name: "DiselLitre",
                table: "DailyWork",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<double>(
                name: "DiselAmount",
                table: "DailyWork",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<short>(
                name: "BillId",
                table: "DailyWork",
                nullable: true,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "AdvanceAmount",
                table: "DailyWork",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AddColumn<short>(
                name: "SiteId",
                table: "DailyWork",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AlterColumn<short>(
                name: "Id",
                table: "Bills",
                nullable: false,
                oldClrType: typeof(int))
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn)
                .OldAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.CreateTable(
                name: "Sites",
                columns: table => new
                {
                    Id = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sites", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Sites");

            migrationBuilder.DropColumn(
                name: "SiteId",
                table: "DailyWork");

            migrationBuilder.AlterColumn<double>(
                name: "DiselLitre",
                table: "DailyWork",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "DiselAmount",
                table: "DailyWork",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "BillId",
                table: "DailyWork",
                nullable: true,
                oldClrType: typeof(short),
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "AdvanceAmount",
                table: "DailyWork",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AddColumn<double>(
                name: "TotalHours",
                table: "DailyWork",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<string>(
                name: "VechicleNumber",
                table: "DailyWork",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Id",
                table: "Bills",
                nullable: false,
                oldClrType: typeof(short))
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn)
                .OldAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);
        }
    }
}
