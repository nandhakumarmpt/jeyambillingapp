﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace JeyamBillingApp.Data.Migrations
{
    public partial class DailyWorkTimeColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<TimeSpan>(
                name: "StartTime",
                table: "DailyWork",
                nullable: false,
                oldClrType: typeof(DateTime));

            migrationBuilder.AlterColumn<TimeSpan>(
                name: "EndDateTime",
                table: "DailyWork",
                nullable: false,
                oldClrType: typeof(DateTime));

            migrationBuilder.CreateTable(
                name: "Bills",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SiteName = table.Column<string>(nullable: true),
                    IsMonthlyBill = table.Column<bool>(nullable: false),
                    HourlyRate = table.Column<double>(nullable: false),
                    MonthlyHoursLimit = table.Column<short>(nullable: false),
                    IsPaymentDone = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bills", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DailyWork_BillId",
                table: "DailyWork",
                column: "BillId");

            migrationBuilder.AddForeignKey(
                name: "FK_DailyWork_Bills_BillId",
                table: "DailyWork",
                column: "BillId",
                principalTable: "Bills",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DailyWork_Bills_BillId",
                table: "DailyWork");

            migrationBuilder.DropTable(
                name: "Bills");

            migrationBuilder.DropIndex(
                name: "IX_DailyWork_BillId",
                table: "DailyWork");

            migrationBuilder.AlterColumn<DateTime>(
                name: "StartTime",
                table: "DailyWork",
                nullable: false,
                oldClrType: typeof(TimeSpan));

            migrationBuilder.AlterColumn<DateTime>(
                name: "EndDateTime",
                table: "DailyWork",
                nullable: false,
                oldClrType: typeof(TimeSpan));
        }
    }
}
