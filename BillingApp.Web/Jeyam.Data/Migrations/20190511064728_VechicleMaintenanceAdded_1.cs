﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace JeyamBillingApp.Data.Migrations
{
    public partial class VechicleMaintenanceAdded_1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VechicleMaintenance_Vechicles_VechicleId",
                table: "VechicleMaintenance");

            migrationBuilder.DropPrimaryKey(
                name: "PK_VechicleMaintenance",
                table: "VechicleMaintenance");

            migrationBuilder.RenameTable(
                name: "VechicleMaintenance",
                newName: "VechicleMaintenances");

            migrationBuilder.RenameIndex(
                name: "IX_VechicleMaintenance_VechicleId",
                table: "VechicleMaintenances",
                newName: "IX_VechicleMaintenances_VechicleId");

            migrationBuilder.AddColumn<double>(
                name: "Amount",
                table: "VechicleMaintenances",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_VechicleMaintenances",
                table: "VechicleMaintenances",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_VechicleMaintenances_Vechicles_VechicleId",
                table: "VechicleMaintenances",
                column: "VechicleId",
                principalTable: "Vechicles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VechicleMaintenances_Vechicles_VechicleId",
                table: "VechicleMaintenances");

            migrationBuilder.DropPrimaryKey(
                name: "PK_VechicleMaintenances",
                table: "VechicleMaintenances");

            migrationBuilder.DropColumn(
                name: "Amount",
                table: "VechicleMaintenances");

            migrationBuilder.RenameTable(
                name: "VechicleMaintenances",
                newName: "VechicleMaintenance");

            migrationBuilder.RenameIndex(
                name: "IX_VechicleMaintenances_VechicleId",
                table: "VechicleMaintenance",
                newName: "IX_VechicleMaintenance_VechicleId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_VechicleMaintenance",
                table: "VechicleMaintenance",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_VechicleMaintenance_Vechicles_VechicleId",
                table: "VechicleMaintenance",
                column: "VechicleId",
                principalTable: "Vechicles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
