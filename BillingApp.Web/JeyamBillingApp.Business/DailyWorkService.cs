﻿using JeyamBillingApp.Business.Interfaces;
using JeyamBillingApp.Web.Model;
using JeyamBillingApp.Data;
using System;
using System.Collections.Generic;
using System.Text;
using JeyamBillingApp.Data.Repository;
using JeyamBillingApp.Data.Repository.Interfaces;
using System.Linq;

namespace JeyamBillingApp.Business
{
    public class DailyWorkService : IDailyWorkService
    {
        private readonly IDailyWorkRepository _dailyWorkRepository;
        private readonly IVechicleRepository _vechicleRepository;
        private readonly ISiteRepository _siteRepository;

        public DailyWorkService(IDailyWorkRepository dailyWorkRepository, IVechicleRepository vechicleRepository, ISiteRepository siteRepository)
        {
            _dailyWorkRepository = dailyWorkRepository;
            _vechicleRepository = vechicleRepository;
            _siteRepository = siteRepository;
        }

        public DailyWorkViewModel Add(DailyWorkViewModel dailyWorkViewModel)
        {
            var dailyWorkEntity = new DailyWork();
            dailyWorkEntity.AdvanceAmount = dailyWorkViewModel.AdvanceAmount;
            dailyWorkEntity.Date = dailyWorkViewModel.Date;
            dailyWorkEntity.DiselAmount = dailyWorkViewModel.DiselAmount;
            dailyWorkEntity.DiselLitre = dailyWorkViewModel.DiselLitre;
            dailyWorkEntity.EndTime = dailyWorkViewModel.EndTime;
            dailyWorkEntity.StartTime = dailyWorkViewModel.StartTime;
            dailyWorkEntity.SiteId = dailyWorkViewModel.SiteId;
            dailyWorkEntity.VechicleId = dailyWorkViewModel.VechicleId;
            _dailyWorkRepository.Add(dailyWorkEntity);
            dailyWorkViewModel.DailyWorkId = dailyWorkEntity.Id;
            return dailyWorkViewModel;
        }

        public void Delete(DailyWorkViewModel dailyWorkViewModel)
        {
            var dailyWorkEntity = _dailyWorkRepository.GetDailyWorks().FirstOrDefault(s => s.Id == dailyWorkViewModel.DailyWorkId);
            if (dailyWorkEntity != null)
            {
                _dailyWorkRepository.Delete(dailyWorkEntity);
            }
        }

        public List<DailyWorkViewModel> GetDailyWorks()
        {
            var result = (from dailyWork in _dailyWorkRepository.GetDailyWorks()
                          join vechicle in _vechicleRepository.GetVechicles()
                           on dailyWork.VechicleId equals vechicle.Id

                           join site in _siteRepository.GetSites() 
                            on dailyWork.SiteId equals site.Id

                          orderby dailyWork.Date descending, dailyWork.StartTime descending

                          select new DailyWorkViewModel()
                          {
                              DailyWorkId = dailyWork.Id,
                              AdvanceAmount = dailyWork.AdvanceAmount,
                              BillId = dailyWork.BillId,
                              Date = dailyWork.Date,
                              DiselAmount = dailyWork.DiselAmount,
                              DiselLitre = dailyWork.DiselLitre,
                              EndTime = dailyWork.EndTime,
                              StartTime = dailyWork.StartTime,
                              VechicleId = dailyWork.VechicleId,
                              VechicleNumber = vechicle.Model,
                              SiteName = site.Name,
                              SiteId = dailyWork.SiteId,

                          }).ToList();

            return result;

        }

        public DailyWorkViewModel Update(DailyWorkViewModel dailyWorkViewModel)
        {
            var dailyWorkEntity = _dailyWorkRepository.GetDailyWorks().FirstOrDefault(s => s.Id == dailyWorkViewModel.DailyWorkId);
            if (dailyWorkEntity != null)
            {
                dailyWorkEntity.AdvanceAmount = dailyWorkViewModel.AdvanceAmount;
                dailyWorkEntity.Date = dailyWorkViewModel.Date;
                dailyWorkEntity.DiselAmount = dailyWorkViewModel.DiselAmount;
                dailyWorkEntity.DiselLitre = dailyWorkViewModel.DiselLitre;
                dailyWorkEntity.EndTime = dailyWorkViewModel.EndTime;
                dailyWorkEntity.StartTime = dailyWorkViewModel.StartTime;
                dailyWorkEntity.SiteId = dailyWorkViewModel.SiteId;
                dailyWorkEntity.VechicleId = dailyWorkViewModel.VechicleId;
                _dailyWorkRepository.Update(dailyWorkEntity);
            }

            return dailyWorkViewModel;
        }

        public List<PicklistViewModel> GetSitesName()
        {
            var result = (from site in _siteRepository.GetSites()
                          orderby site.Name
                          select new PicklistViewModel()
                          {
                              Id = site.Id,
                              Text = site.Name
                          }).ToList();

            return result;
        }
    }
}
