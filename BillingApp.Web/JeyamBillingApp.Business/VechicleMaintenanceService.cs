﻿using JeyamBillingApp.Business.Interfaces;
using JeyamBillingApp.Web.Model;
using JeyamBillingApp.Data;
using System;
using System.Collections.Generic;
using System.Text;
using JeyamBillingApp.Data.Repository;
using JeyamBillingApp.Data.Repository.Interfaces;
using System.Linq;

namespace JeyamBillingApp.Business
{
    public class VechicleMaintenanceService : IVechicleMaintenanceService
    {

        private readonly IVechicleMaintenanceRepository _vechicleMaintenanceRepository;
        private readonly IVechicleRepository _vechicleRepository;

        public VechicleMaintenanceService(IVechicleMaintenanceRepository vechicleMaintenanceRepository, IVechicleRepository vechicleRepository)
        {
            _vechicleMaintenanceRepository = vechicleMaintenanceRepository;
            _vechicleRepository = vechicleRepository;
        }

        public VechicleMaintenanceViewModel Add(VechicleMaintenanceViewModel vechicleMaintenanceViewModel)
        {
            if (vechicleMaintenanceViewModel != null)
            {
                var maintenanceEntity = new VechicleMaintenance();
                maintenanceEntity.Date = vechicleMaintenanceViewModel.Date;
                maintenanceEntity.Note = vechicleMaintenanceViewModel.Note;
                maintenanceEntity.VechicleId = vechicleMaintenanceViewModel.VechicleId;
                maintenanceEntity.Amount = vechicleMaintenanceViewModel.Amount;
                _vechicleMaintenanceRepository.Add(maintenanceEntity);
                vechicleMaintenanceViewModel.VechicleMaintenanceId = maintenanceEntity.Id;
                updateVechicleNumber(ref vechicleMaintenanceViewModel);
            }

            return vechicleMaintenanceViewModel;
        }


        public void Delete(VechicleMaintenanceViewModel vechicleMaintenanceViewModel)
        {
            var maintenanceEntity = _vechicleMaintenanceRepository.GetVechicleMaintenances().FirstOrDefault(s => s.Id == vechicleMaintenanceViewModel.VechicleMaintenanceId);
            if (maintenanceEntity != null)
            {
                _vechicleMaintenanceRepository.Delete(maintenanceEntity);
            }
        }

        public VechicleMaintenanceViewModel GetVechicleMaintenance(int id)
        {
            throw new NotImplementedException();
        }

        public List<VechicleMaintenanceViewModel> GetVechicleMaintenances()
        {
            var result = (from vechicle in _vechicleRepository.GetVechicles()
                          join maintenance in _vechicleMaintenanceRepository.GetVechicleMaintenances() on vechicle.Id equals maintenance.VechicleId

                          select new VechicleMaintenanceViewModel()
                          {
                              Amount = maintenance.Amount,
                              Date = maintenance.Date,
                              Note = maintenance.Note,
                              VechicleId = vechicle.Id,
                              VechicleMaintenanceId = maintenance.Id,
                              VechicleNumber = vechicle.Model
                          }).ToList();

            return result;
        }

        public VechicleMaintenanceViewModel Update(VechicleMaintenanceViewModel vechicleMaintenanceViewModel)
        {
            if (vechicleMaintenanceViewModel != null)
            {
                var maintenanceEntity = _vechicleMaintenanceRepository.GetVechicleMaintenances().FirstOrDefault(s => s.Id == vechicleMaintenanceViewModel.VechicleMaintenanceId);
                if (maintenanceEntity != null)
                {
                    maintenanceEntity.Date = vechicleMaintenanceViewModel.Date;
                    maintenanceEntity.Note = vechicleMaintenanceViewModel.Note;
                    maintenanceEntity.VechicleId = vechicleMaintenanceViewModel.VechicleId;
                    maintenanceEntity.Amount = vechicleMaintenanceViewModel.Amount;
                    _vechicleMaintenanceRepository.Update(maintenanceEntity);

                    updateVechicleNumber(ref vechicleMaintenanceViewModel);
                }
            }
            return vechicleMaintenanceViewModel;
        }


        public List<PicklistViewModel> GetVechiclesList()
        {
            var result = (from vechicle in _vechicleRepository.GetVechicles()
                          orderby vechicle.Model
                          select new PicklistViewModel()
                          {
                              Id = vechicle.Id,
                              Text = vechicle.Model
                          }).ToList();

            return result;
        }

        private void updateVechicleNumber(ref VechicleMaintenanceViewModel vechicleViewModel)
        {
            var vechile = _vechicleRepository.GetVechicle(vechicleViewModel.VechicleId);
            vechicleViewModel.VechicleNumber = vechile.Model;
        }
    }
}
