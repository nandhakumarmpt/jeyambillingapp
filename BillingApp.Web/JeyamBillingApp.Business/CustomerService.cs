﻿using System;
using System.Collections.Generic;
using System.Text;
using JeyamBillingApp.Business.Interfaces;
using JeyamBillingApp.Data;
using JeyamBillingApp.Web.Model;
using JeyamBillingApp.Data.Repository.Interfaces;
using System.Linq;

namespace JeyamBillingApp.Business
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;

        public CustomerService(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        public CustomerViewModel Add(CustomerViewModel customerViewModel)
        {
            var customerEntity = new Customer();
            customerEntity.City = customerViewModel.City;
            customerEntity.Name = customerViewModel.Name;
            customerEntity.PIN = customerViewModel.PIN;
            _customerRepository.Add(customerEntity);
            customerViewModel.CustomerId = customerEntity.Id;
            return customerViewModel;
        }

        public void Delete(CustomerViewModel customerViewModel)
        {
            var customerEntity = _customerRepository.GetCustomers().FirstOrDefault(d => d.Id == customerViewModel.CustomerId);
            if (customerEntity != null)
            {
                _customerRepository.Delete(customerEntity);
            }
        }

        public List<CustomerViewModel> GetCustomers()
        {
            var result = (from customer in _customerRepository.GetCustomers()
                          select new CustomerViewModel()
                          {
                              City = customer.City,
                              CustomerId = customer.Id,
                              Name = customer.Name,
                              PIN = customer.PIN
                          }).ToList();

            return result;
        }

        public CustomerViewModel Update(CustomerViewModel customerViewModel)
        {
            var customerEntity = _customerRepository.GetCustomers().FirstOrDefault(d => d.Id == customerViewModel.CustomerId);
            if (customerEntity != null)
            {
                customerEntity.City = customerViewModel.City;
                customerEntity.Name = customerViewModel.Name;
                customerEntity.PIN = customerViewModel.PIN;
                _customerRepository.Update(customerEntity);
            }

            return customerViewModel;
        }
    }
}
