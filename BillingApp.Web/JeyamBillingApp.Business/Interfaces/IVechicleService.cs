﻿using JeyamBillingApp.Web.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JeyamBillingApp.Business.Interfaces
{
    public interface IVechicleService
    {
        VechicleViewModel GetVechicle(int vechicleId);

        List<VechicleViewModel> GetVechicles();

        VechicleViewModel Add(VechicleViewModel vechicle);

        VechicleViewModel Update(VechicleViewModel vechicle);

        int Delete(int vechicleId);

        bool DoesVechicleExists(string modelName, int id = 0);
    }
}
