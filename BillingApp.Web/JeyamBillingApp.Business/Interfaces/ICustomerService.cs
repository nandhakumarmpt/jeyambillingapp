﻿using JeyamBillingApp.Web.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace JeyamBillingApp.Business.Interfaces
{
    public interface ICustomerService
    {
        List<CustomerViewModel> GetCustomers();

        CustomerViewModel Add(CustomerViewModel customerViewModel);

        CustomerViewModel Update(CustomerViewModel customerViewModel);

        void Delete(CustomerViewModel customerViewModel);

    }
}
