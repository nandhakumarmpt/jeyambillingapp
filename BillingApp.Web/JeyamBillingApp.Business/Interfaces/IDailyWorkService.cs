﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using JeyamBillingApp.Web.Model;

namespace JeyamBillingApp.Business.Interfaces
{
    public interface IDailyWorkService
    {
        List<DailyWorkViewModel> GetDailyWorks();

        DailyWorkViewModel Add(DailyWorkViewModel dailyWorkViewModel);

        DailyWorkViewModel Update(DailyWorkViewModel dailyWorkViewModel);

        void Delete(DailyWorkViewModel dailyWorkViewModel);

        List<PicklistViewModel> GetSitesName();
    }
}
