﻿using JeyamBillingApp.Web.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace JeyamBillingApp.Business.Interfaces
{
    public interface IVechicleMaintenanceService
    {
        VechicleMaintenanceViewModel GetVechicleMaintenance(int id);

        List<VechicleMaintenanceViewModel> GetVechicleMaintenances();

        VechicleMaintenanceViewModel Add(VechicleMaintenanceViewModel vechicleMaintenanceViewModel);

        VechicleMaintenanceViewModel Update(VechicleMaintenanceViewModel vechicleMaintenanceViewModel);

        void Delete(VechicleMaintenanceViewModel vechicleMaintenanceViewModel);

        List<PicklistViewModel> GetVechiclesList();
    }
}
