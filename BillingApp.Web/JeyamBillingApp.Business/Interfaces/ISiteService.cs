﻿using JeyamBillingApp.Web.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace JeyamBillingApp.Business.Interfaces
{
   public interface ISiteService
    {
        List<SiteViewModel> GetSites();

        SiteViewModel Add(SiteViewModel siteViewModel);

        SiteViewModel Update(SiteViewModel siteViewModel);

        void Delete(SiteViewModel siteViewModel);
    }
}
