﻿using JeyamBillingApp.Business.Interfaces;
using JeyamBillingApp.Data;
using JeyamBillingApp.Data.Repository.Interfaces;
using JeyamBillingApp.Web.Model;
using System.Collections.Generic;
using System.Linq;

namespace JeyamBillingApp.Business
{
    public class SiteService : ISiteService

    {
        private readonly ISiteRepository _siteRepository;

        public SiteService(ISiteRepository siteRepository)
        {
            _siteRepository = siteRepository;
        }

        public SiteViewModel Add(SiteViewModel siteViewModel)
        {
            var siteEntity = new Site();
            siteEntity.Name = siteViewModel.Name;
            _siteRepository.Add(siteEntity);
            siteViewModel.SiteId = siteEntity.Id;
            return siteViewModel;
        }

        public void Delete(SiteViewModel siteViewModel)
        {
            var siteEntity = _siteRepository.GetSites().FirstOrDefault(d => d.Id == siteViewModel.SiteId);
            if (siteEntity != null)
            {
                _siteRepository.Delete(siteEntity);
            }
        }

        public List<SiteViewModel> GetSites()
        {
            return _siteRepository.GetSites().Select(s => new SiteViewModel()
            {
                Name = s.Name,
                SiteId = s.Id
            }).OrderBy(s => s.Name).ToList();
        }

        public SiteViewModel Update(SiteViewModel siteViewModel)
        {
            var siteEntity = _siteRepository.GetSites().FirstOrDefault(d => d.Id == siteViewModel.SiteId);
            if (siteEntity!=null)
            {
                siteEntity.Name = siteViewModel.Name;
                _siteRepository.Update(siteEntity);
            }
            return siteViewModel;
        }
    }
}
