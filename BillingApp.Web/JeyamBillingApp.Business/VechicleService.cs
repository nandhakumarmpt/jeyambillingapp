﻿using System.Collections.Generic;
using JeyamBillingApp.Business.Interfaces;
using JeyamBillingApp.Web.Model;
using JeyamBillingApp.Data.Repository.Interfaces;
using Jeyam.Data;
using System.Linq;

namespace JeyamBillingApp.Business
{
    public class VechicleService : IVechicleService
    {
        private readonly IVechicleRepository _vechicleRepository;

        public VechicleService(IVechicleRepository vechicleRepository)
        {
            _vechicleRepository = vechicleRepository;
        }

        public VechicleViewModel Add(VechicleViewModel vechicle)
        {
            var vechicleObj = new Vechicle();
            vechicleObj.Model = vechicle.VechicleNumber;
            vechicleObj = _vechicleRepository.Add(vechicleObj);
            vechicle.VechicleId = vechicleObj.Id;

            return vechicle;
        }

        public int Delete(int vechicleId)
        {
            var vechicleEntity = _vechicleRepository.GetVechicles().FirstOrDefault(s => s.Id == vechicleId);
            if (vechicleEntity != null)
            {
                _vechicleRepository.Delete(vechicleEntity);
            }
            return 0;
        }

        public bool DoesVechicleExists(string modelName, int id = 0)
        {
            return _vechicleRepository.GetVechicles().Any(s => s.Model == modelName && s.Id == (id == 0 ? s.Id : id));
        }

        public VechicleViewModel GetVechicle(int vechicleId)
        {
            var vechicleEntity = _vechicleRepository.GetVechicles().FirstOrDefault(s => s.Id == vechicleId);
            if (vechicleEntity != null)
            {
                return new VechicleViewModel()
                {
                    VechicleId = vechicleEntity.Id,
                    VechicleNumber = vechicleEntity.Model
                };
            }
            else
                return null;
        }

        public List<VechicleViewModel> GetVechicles()
        {
            var vechicleEntityList = _vechicleRepository.GetVechicles().ToList();

            return (from vechicle in vechicleEntityList
                    select new VechicleViewModel()
                    {
                        VechicleId = vechicle.Id,
                        VechicleNumber = vechicle.Model
                    }).ToList();
        }

        public VechicleViewModel Update(VechicleViewModel vechicle)
        {
            var vechicleObj = _vechicleRepository.GetVechicles().FirstOrDefault(s => s.Id == vechicle.VechicleId);
            if (vechicleObj != null)
            {
                vechicleObj.Model = vechicle.VechicleNumber;
                vechicleObj = _vechicleRepository.Update(vechicleObj);
            }
            return vechicle;
        }
    }
}
